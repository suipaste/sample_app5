require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:michael)
  end

  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: @user.name
    assert_select 'h1>img.gravatar'
    assert_match @user.microposts.count.to_s, response.body
    assert_select 'div.pagination'
    @user.microposts.paginate(page:1 ).each do |micropost|
      assert_match micropost.content, response.body
    end
  end

  test "home page display" do
    # Check displays 'logged out' home page when not logged in
    get root_path
    assert_template 'static_pages/home'
    assert_select 'h1', text: 'Welcome to the Sample App'

    # Log in
    log_in_as(@user)

    # Check the home page is now personalised
    get root_path
    assert_template 'static_pages/home'
    assert_select 'h1', text: @user.name
    assert_select 'strong#following.stat', text: '2'
    assert_select 'strong#followers.stat', text: '2'

    assert_select 'textarea#micropost_content'
  end
end
